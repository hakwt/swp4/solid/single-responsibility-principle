# Single-Responsibility Principle

1. Betrachte die Klasse `TextManipulator`. Wie viele Gründe könnte es geben diese Klassen zu ändern?
2. Wenn du eine Idee hast, versuche den Sourcecode so zu ändern, dass `TextManipulator` nur mehr einen
   Grund hat um geändert zu werden. Der Code in der Main-Klasse sollte dabei unverändert bleiben.
3. Dokumentiere als Javadoc in der Klasse `Main` wie es mit Kopplung und Kohäsion in deiner Lösung beschaffen ist.
4. Commite und pushe deine Ergebnisse
